Roipoussiere's dotfiles
=======================

## Usage

### Downloading dotfiles

```
git clone git@framagit.org:roipoussiere/dotfiles.git
cd dotfiles
```

### Updating user config files:

This script will create a symbolic link for each configuration file, and if one of them exists,
will ask you to replace it or not, and copy the old config as backup.

```
./update.sh
```

### Updating system config files (manually for security reasons)

```
cp /etc/sddm.conf /etc/sddm.conf.old
sudo ln -s $HOME/.dotfiles/root/etc/sddm.conf /etc/sddm.conf
```

### Other configurations steps

- `systemctl enable sddm.service --force`
- in file `/etc/lightdm/lightdm.conf`, look for `user-session` and edit to `user-session=sway`
- in file `/etc/profile`, add the line `export _JAVA_AWT_WM_NONREPARENTING=1`
