#!/usr/bin/env zsh

#           _              
#   _______| |__  _ __ ___ 
#  |_  / __| '_ \| '__/ __|
# _ / /\__ \ | | | | | (__ 
#(_)___|___/_| |_|_|  \___|

###################
### cheat-sheet ###
###################

# <esc>-h: display man page of the first word of the current command
# <esc>-q: copy current command, erase it, then paste it after the next command is executed

# read https://wiki.archlinux.org/index.php/Zsh to master zsh

####################
### shell config ###
####################

# exporting variables for zsh and extensions
export TERM='xterm-256color'
export TERM_EMU='kitty' # default terminal emulator
export EDITOR='nvim' # default text editor
export HISTFILE="$HOME/.zsh_history"
export HISTSIZE=10000
export SAVEHIST=10000
export POWERLEVEL9K_MODE='awesome-fontconfig'
export SUB_ZSHRC="$HOME/._zshrc"
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib/
export PIP_REQUIRE_VIRTUALENV=true # Prevent system pip mess
export PATH=$PATH:~/.local/bin # Allow executions of pip packages
export LANG=fr_FR.UTF-8

# modules paths
fpath+=~/.zfunc
fpath+=~/.zsh/completion

# loading zsh modules
autoload -Uz compinit \
             up-line-or-beginning-search \
             down-line-or-beginning-search
compinit

# enabling other useful features
zstyle ':completion:*' menu select
zle -N up-line-or-beginning-search
zle -N down-line-or-beginning-search

# setting zsh options - see http://zsh.sourceforge.net/Doc/Release/Options.html
setopt SHARE_HISTORY \
       INTERACTIVE_COMMENTS \
       NO_BEEP \
       COMPLETE_ALIASES

# sourcing theme and extensions
source /usr/share/zsh-theme-powerlevel9k/powerlevel9k.zsh-theme
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
if [[ -f $SUB_ZSHRC ]]; then
    source $SUB_ZSHRC
fi

# setting key bindings (for st-term)
bindkey '^[[1;5D' backward-word
bindkey '^[[1;5C' forward-word
bindkey '^[[3~' delete-char
bindkey '^[[6~' end-of-buffer-or-history
bindkey '^[[H' beginning-of-line
bindkey '^[[F' end-of-line
bindkey '^[[A' up-line-or-beginning-search
bindkey '^[[B' down-line-or-beginning-search

# key binding tips:
# - `ctrl-v` followed by a sequence code to display its code
# - `bindkey -l` to list existing keymap names
# - `bindkey -M <keymap>` to list all the bindings in a given keymap

###############
### ALIASES ###
###############

# ls aliases
alias la="ls -AlhX"
alias lr="ls -R"

# Pipe aliases (end of command)
alias -g H="| head"
alias -g T="| tail"
alias -g G="| grep"
alias -g L="| less"
alias -g N='| notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# Redirection aliases (end of command)
alias -g LL="2>&1 | less"
alias -g CA="2>&1 | cat -A"
alias -g NE="2> /dev/null"
alias -g NUL="> /dev/null 2>&1"

# apt-get aliases
alias apts="apt-cache search"
alias apti="sudo apt install"
alias aptu="sudo apt update"
alias aptr="sudo apt remove"

# Git aliases
#alias gl="git log --graph --pretty=oneline --abbrev-commit"
alias gl='git log --graph --pretty=format:"%Cred%h %Cblue%an %Cgreen%ar %Cred%D %Creset%s"'
alias gs="git status"
alias gS="git stash -u"
alias gc="git checkout"
alias gC="git status && echo '---' && git commit -m"
alias gCa="git commit --amend"
alias ga="git add"
alias gd="git diff"
alias gD="git diff --cached"
alias gp="git pull --rebase"
alias gP="git status && echo '---' && git push"
alias gb="git branch"
alias gM="git merge"

# Docker aliases
alias dp="docker ps -a"
alias dr="docker run"
alias de="docker exec -it"
alias db="docker build"
alias dl="docker logs -f"
alias dR="docker rm -f"
alias dRa='docker rm -f $(docker ps -aq)'
alias di="docker images"
alias diR="docker rmi"
alias diRa='docker rmi $(docker images --filter dangling=true -q --no-trunc)'
alias ds="docker start"
alias dS="docker stop"
alias dv="docker volume"
alias dvc="docker volume create"
alias dvR="docker volume rm"
alias dvRa='docker volume rm $(docker volume list -q)'
alias dc="docker-compose"

# Shortcuts for installed programs
alias h="howdoi -c -n5" # see https://github.com/gleitz/howdoi

# Misc.
alias zrc="$EDITOR ~/.zshrc"
alias f='find . -name '
alias ht='history -iD | tail -50'
alias hg='history -iD | grep '
alias screencast="ffmpeg -f x11grab -s 1920x1080 -r 25 -i :0.0 ~/Vidéos/screencast_$(date +%Y-%m-%d_%H:%I:%S).mp4"
alias vim='nvim'
alias xo='xdg-open' # see https://wiki.archlinux.org/index.php/Xdg-utils#xdg-open and https://wiki.archlinux.org/index.php/XDG_MIME_Applications
alias ffpro='firefox -P "pro"'
alias ffperso='firefox -P "perso"'
